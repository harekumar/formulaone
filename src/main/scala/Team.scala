/**
  * Created by style on 17/03/17.
  */
class Team(val n:String, val c:List[Car]) {

  private[this] var _name: String = n

  private[this] var _cars: List[Car] = c

  def name: String = _name

  def name_=(value: String): Unit = {
    _name = value
  }

  def cars: List[Car] = _cars

  def cars_=(value: List[Car]): Unit = {
    _cars = value
  }

  override def toString = s"Team(name=$name, cars=$cars)"
}
