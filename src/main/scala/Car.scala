/**
  * Created by style on 17/03/17.
  */
class Car(val n:String){

  var name:String = n

  var carNo:Int = 0
  var acceleration:Double = 0.0
  var currentSpeed:Double = 0.0
  var maxSpeed:Double = 0.0

  var startingPosition:Int = 0

  var distanceCovered:Double = 0.0
  var isRaceFinished:Boolean = false

  var timeTakenToFinishRace:Int = 0
  var rank:Int = 0


  override def toString = s"Car(carNo=$carNo, currentSpeed=$currentSpeed, maxSpeed=$maxSpeed, distanceCovered=$distanceCovered, isRaceFinished=$isRaceFinished, timeTakenToFinishRace=$timeTakenToFinishRace, rank=$rank)"
}
