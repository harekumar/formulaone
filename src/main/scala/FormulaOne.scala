import scala.collection.mutable

/**
  * Created by style on 17/03/17.
  */

object FormulaOne  {
  /**
    *  Top speed = (150 + 10 * i) Km/h
    *  Acceleration = (2 * i) meter per second square
    *  Handling Factor (hf) = (0.8 * Current Car Speed) if the car is in range of 10 meter to any other car.
    *  Nitro : Doubles the current speed or top speed whichever is lower
    *
    *  Cars starting position: (200 * i) meter from the first car
    *  Total teams = Take input from command line (Min 1)
    *  Length of track = Take input from command line
    *
    *
    */

  /**
    * Initial values:
    * Track Length = 10 KM i.e converted to Meter
    * Team Size = 8
    * Assessment period = 2 second
    */
  val trackLen:Double = 10 * 1000
  val teamLength:Int = 8
  val assesmentPeriod:Int = 2

  var raceFinishedCounter:Int = 0
  var raceFinished:Boolean = false

  var teams:List[Team] = List.empty

  def main(args: Array[String]): Unit = {

    for(i <- 1 to teamLength) {
      teams = teams ::: List(setTeamDetails(i))
    }

    // Get all cars & store it in a List
    var cars:List[Car] = List.empty
    teams.foreach(t => t.cars.foreach(c => cars = cars:::List(c)))

    // Initialize time counter
    var time:Int = 0

    while(!raceFinished) {

      /** Distance covered:
        * S = ut + (1/2)*(acceleration)*(time)
        * Since we have to revisit the position of each car every two seconds so the time should increase by 2 seconds
        */

      for (car <- cars) {

        var s:Double = 0.0
        var v:Double = 0.0

        if (!car.isRaceFinished) {

          if (car.currentSpeed.<=(car.maxSpeed)) {

            // Velocity = Initial Velocity + acceleration * time i.e v = u + at
            v = ((convertToMeterPerSecond(car.currentSpeed)) + (car.acceleration * time))

            if((convertToKiloMeterPerSecond(v)).>=(car.maxSpeed)){
              car.currentSpeed = car.maxSpeed
            } else {
              car.currentSpeed = (v * 3600) / 1000
            }

          }

          // Distance = Velocity * time i.e s = v * t
          s = car.currentSpeed * 2

          car.distanceCovered = car.distanceCovered + s

          if (car.distanceCovered >= trackLen) {
            car.isRaceFinished = true
            raceFinishedCounter += 1
          }

          car.timeTakenToFinishRace = time

          /** Do assessment
            * 1. Check if there is any other car in range of 10 meter if so apply handling factor & reduce the speed.
            * 2. Check if the driver is the last one then apply nitro
            */
          if (!car.isRaceFinished)
            runAssesment(car, cars)

          if (!car.isRaceFinished)
            runNitrousCheck(car)
        }

      }
      if (raceFinishedCounter == teamLength)
        raceFinished = true

      time += assesmentPeriod
      rankCar(time, cars)

      println()
      println("--------------------")
      for(team <- teams)
        println(team.toString)
      println("--------------------")
    }

  }

  def rankCar(t: Int, cars: List[Car]): Unit = {
    // At given time check the position of all the car & rearrange the position
    val checkPoint:Double = trackLen
    var hashMap:mutable.ListMap[Int, Double] = mutable.ListMap.empty

    cars.foreach(c => hashMap += c.carNo -> (checkPoint - c.distanceCovered))

    // Get cars as set so that it can be easily retrieved with given key
    var hashMapCars:mutable.ListMap[Int, Car] = mutable.ListMap.empty
    cars.foreach(c => hashMapCars += c.carNo -> c)

    var i:Int = 0
    for(h <- hashMap.toList.sortWith(_._2 < _._2)) {
      //var j:Int = 0
      // If the checkpoint difference is greater than zero then it means car is still in race
      if (h._2 > 0) hashMapCars.get(h._1).map(c => c.rank = i + 1)

      i += 1
    }

  }

  def runNitrousCheck(car: Car): Unit = {

    // Assuming each team has only one car in a race. Then the rank of the last car will be equal to total no of teams
    if (car.rank.equals(teams.length)) {
      // Apply nitrous. Hence the car speed = Min(currentSpeed * 2, carMaxSpeed)
      println(s"Applying nitro to car $car")
      car.currentSpeed = Math.min(car.currentSpeed * 2, car.maxSpeed)
    }
  }

  def runAssesment(car: Car, cars: List[Car]): Unit = {

    for (c <- cars) {
      if (!c.carNo.equals(car.carNo)) {

        // If in 10 meter range apply Handling factor i.e currentSpeed = 0.8 * currentSpeed
        if (checkIfInRangeOf10Meter(c.distanceCovered, car.distanceCovered)) {
          println(s"Handling factor applying on car no $car")
          car.currentSpeed = 0.8 * car.currentSpeed
          return
        }
      }
    }

  }

  def checkIfInRangeOf10Meter(d1: Double, d2: Double): Boolean = {

    var isInRange:Boolean = false

    if (Math.abs(d1 - d2) <= 10)
      isInRange = true

    return isInRange
  }

  def convertToMeterPerSecond(v: Double): Double = {
    return (v * 1000)/3600
  }

  def convertToKiloMeterPerSecond(v: Double): Double = {
    return (v * 3600)/1000
  }

  def setTeamDetails(i: Int): Team = {

    var carList:List[Car]=Nil

    // String interpolation
    val c1 = new Car(s"C$i")

    c1.carNo = i
    c1.acceleration= (2 * i)
    c1.currentSpeed=0
    c1.maxSpeed=(150 + 10 * i)

    if(i > 1) {
      c1.startingPosition = -(200 * i)
    }else {
      c1.startingPosition = 0
    }
    c1.distanceCovered = c1.startingPosition

    // Add item to the list
    carList = carList ::: List(c1)
    val t1 = new Team(s"T$i", carList)

    return t1
  }

}
